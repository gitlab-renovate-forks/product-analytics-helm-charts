# Product Analytics helm charts

This repo contains the configuration examples and helm charts for the Product Analytics infrastructure.

Please refer to [our documentation](https://docs.gitlab.com/ee/user/product_analytics/#how-product-analytics-works) for how product analytics works.

These helm charts should be used to set up your own deployment of the Product Analytics infrastructure.
GitLab uses the same helm charts for it's own managed-service deployments.

### Prerequisites

1. Ensure you have access to the GCP project that you aim to deploy to.
1. Ensure you have [`helm`](https://formulae.brew.sh/formula/helm#default),
   [`google-cloud-sdk (gcloud)`](https://formulae.brew.sh/cask/google-cloud-sdk),
   [`go-task`](https://formulae.brew.sh/formula/go-task),
   and [`kubernetes-cli (kubectl)`](https://formulae.brew.sh/formula/kubernetes-cli#default) installed.
   - Set up your Google Cloud account via `gcloud init`
   - Set up the GKE auth plugin for `kubectl`:
   ```sh
   gcloud components install gke-gcloud-auth-plugin
   export USE_GKE_GCLOUD_AUTH_PLUGIN=True
   ```

## Documentation

- [Contribution Guidelines](./CONTRIBUTING.md)
- [Architecture](./docs/architecture.md)
- [Installing with GCP](./docs/installation.md)
- [Upgrading with GCP](./docs/upgrade.md)
- [Troubleshooting](./docs/troubleshooting.md)

## Secrets and KMS providers

To keep the values for secrets out of this repository, we **recommend** storing your secrets elsewhere.
Examples of KMS (Key Management Service) providers you may use include:

- [Google Cloud Secret Manager](https://cloud.google.com/security/products/secret-manager)
- [AWS Secrets Manager](https://aws.amazon.com/secrets-manager/)
- [HashiCorp Vault](https://www.vaultproject.io/)