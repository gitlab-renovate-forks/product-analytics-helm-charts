## What does this MR do and why?

<!--
Describe in detail what your merge request does and why.

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

%{first_multiline_commit}

## MR acceptance checklist

- [ ] The correct [type labels](https://handbook.gitlab.com/handbook/engineering/metrics/#work-type-classification) have been applied to this MR.
- [ ] This MR has been made [as small as possible](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#keep-it-simple), to improve review efficiency and code quality.
- [ ] This MR has been self-reviewed per the [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
- [ ] The changes have undergone manual testing and are functioning as intended.
- [ ] This MR has updated the `Chart.yaml` version number following [SemVer versioning practices](https://semver.org/).
- [ ] This MR documents any breaking changes in the MR description, and the upgrade path has been documented in the first commit as well as in MR description.

## How to set up and validate

_Numbered steps to set up and validate the change are strongly suggested._

## How to deploy upon merging

_Numbered steps to explain how this change needs to be deployed. For instance, if there are any changes that should be made outside of the code changes themselves._

/label ~"Category:Product Analytics" ~"devops::monitor" ~"group::platform insights" ~"section::analytics"

/assign me