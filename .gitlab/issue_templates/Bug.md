<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the ~"type::bug" label:

- https://gitlab.com/gitlab-org/analytics-section/product-analytics/helm-charts/-/issues?label_name%5B%5D=type%3A%3Abug

and verify the issue you're about to submit isn't a duplicate.
--->

## Checklist

<!-- Please test the latest versions, that will remove the possibility that you see a bug that is fixed in a newer version. -->

- [ ] I'm using the latest version of the helm charts
    - Chart version: _Put your chart version here_
- [ ] I'm using a version of GitLab that is equal to or later than 16.11
    - GitLab version: _Put your GitLab version here, or say "happens on `gitlab.com`"_

## Summary

<!-- Summarize the bug encountered concisely -->

## Steps to reproduce

<!-- How one can reproduce the issue - this is very important -->

## What is the current _bug_ behavior?

<!-- What actually happens -->

## What is the expected _correct_ behavior?

<!-- What you should see instead -->

## Relevant logs and/or screenshots

## Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

/label ~"type::bug" ~"section::analytics" ~"devops::monitor" ~"group::product analytics" ~"Category:Product Analytics Visualization"