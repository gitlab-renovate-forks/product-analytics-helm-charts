### Custom enrichers

In case of custom enrichers, we are adding them into [custom-enrichers](https://gitlab.com/gitlab-org/analytics-section/product-analytics/custom-enrichers)
repo. This repository acts as the Single Source of Truth (SSOT) for both the [devkit](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit)
and [helm-charts](https://gitlab.com/gitlab-org/analytics-section/product-analytics/helm-charts).