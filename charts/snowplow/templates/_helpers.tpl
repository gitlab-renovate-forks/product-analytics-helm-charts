{{/*
Create the name of the service account to use
*/}}
{{- define "snowplow.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "common.names.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Collector SVC name
*/}}
{{- define "snowplow.collector.serviceName" -}}
{{ include "common.names.fullname" . }}-snowplow-collector
{{- end }}

{{/*
Clickhouse service name
*/}}
{{- define "snowplow.clickhouse.service.name" -}}
{{- if .Values.global.clickhouse.host }}
{{- .Values.global.clickhouse.host }}
{{- else }}
{{- .Release.Name }}-clickhouse
{{- end }}
{{- end }}
