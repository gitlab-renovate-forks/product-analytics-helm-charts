{{/*
Clickhouse service name
*/}}
{{- define "cube.clickhouse.service.name" -}}
{{- if .Values.global.clickhouse.host }}
{{- .Values.global.clickhouse.host }}
{{- else }}
{{- .Release.Name }}-clickhouse
{{- end }}
{{- end }}
