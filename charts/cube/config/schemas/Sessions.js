const {
  securityContext: {
    appId
  }
} = COMPILE_CONTEXT;

cube(`Sessions`, {
  sql: `SELECT * FROM ${appId}.sessions`,
  measures: {
    count: {
      sql: `domain_sessionid`,
      type: `count`,
    },
    averagePerUser: {
      sql: `${count} / ${CUBE.uniqueUsersCount}`,
      type: `number`
    },
    averageDurationMinutes: {
      type: `avg`,
      sql: `(${endAt} - ${startAt}) / 60`
    },
    uniqueUsersCount: {
      type: `countDistinct`,
      sql: `user_id`
    },
    usersCount: {
      type: `count`,
      sql: `user_id`
    }
  },
  dimensions: {
    startAt: {
      sql: `first_timestamp`,
      type: `time`
    },
    userId: {
      sql: `user_id`,
      type: `string`,
    },
    sessionID: {
      sql: `domain_sessionid`,
      type: `string`,
      primary_key: true
    },
    endAt: {
      sql: `last_timestamp`,
      type: `time`
    },
    agentName: {
      sql: `agent_name`,
      type: `string`
    },
    osFamily: {
      sql: `os_family`,
      type: `string`
    },
    osName: {
      sql: `os_name`,
      type: `string`
    },
    osVersion: {
      sql: `os_version`,
      type: `string`
    },
    osVersionMajor: {
      sql: `os_version_major`,
      type: `string`
    },
    agentVersion: {
      sql: `agent_version`,
      type: `string`
    },
    browserLanguage: {
      sql: `browser_language`,
      type: `string`
    },
    documentLanguage: {
      sql: `document_language`,
      type: `string`
    },
    viewportSize: {
      sql: `viewport_size`,
      type: `string`
    }
  }
});
