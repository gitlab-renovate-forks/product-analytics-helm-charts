const {
    CUBEJS_DB_HOST,
    CUBEJS_DB_PORT,
    CUBEJS_DB_USER,
    CUBEJS_DB_PASS,
    CUBEJS_DB_SSL,
    CUBEJS_REFRESH_WORKER,
    CUBEJS_DEV_MODE,
} = process.env;
const useSSL = CUBEJS_DB_SSL?.toLocaleLowerCase() === 'true';
const { FileRepository } = require('@cubejs-backend/server-core');
const { SCHEMA_FILES_LOCATION, CLICKHOUSE_FORMAT } = require('./utils/constants');
const ClickHouseDriver = require('@cubejs-backend/clickhouse-driver');
const { createClient } = require('@clickhouse/client');
const clickhouseClient = createClient({
    host: `${useSSL ? 'https://' : 'http://'}${CUBEJS_DB_HOST}:${CUBEJS_DB_PORT}`,
    username: CUBEJS_DB_USER,
    password: CUBEJS_DB_PASS,
});

const appsQuery = `
  SELECT DISTINCT project_id
  FROM default.active_apps
`;

let config = {
    contextToAppId: ({ securityContext: { appId } }) => `CUBEJS_APP_${appId}`,
    contextToOrchestratorId: ({ securityContext: { appId } }) => `CUBEJS_APP_${appId}`,
    preAggregationsSchema: ({ securityContext: { appId } }) => `pre_aggregations_${appId}`,
    driverFactory: ({ securityContext: { appId } }) => new ClickHouseDriver({
        database: `${appId}`,
    }),
    scheduledRefreshTimer: false,
    scheduledRefreshContexts: () => ({}),
};

if (CUBEJS_REFRESH_WORKER || CUBEJS_DEV_MODE) {
    config = {
        ...config,
        scheduledRefreshTimer: 60, // seconds
        scheduledRefreshContexts: async () => {
            try {
                const rows = await clickhouseClient.query({
                    query: appsQuery,
                    format: CLICKHOUSE_FORMAT,
                });
                const apps = await rows.json();

                return apps.map((app) => {
                    return { securityContext: { appId: app.project_id } }
                });
            } catch (e) {
                console.info(e);
                return [];
            }
        },
        repositoryFactory: ({ securityContext: { appId } }) => ({
            async dataSchemaFiles () {
                const localFiles = new FileRepository(SCHEMA_FILES_LOCATION);

                return [
                    ...await localFiles.dataSchemaFiles(),
                ]
            }
        }),
    };
}

module.exports = config;