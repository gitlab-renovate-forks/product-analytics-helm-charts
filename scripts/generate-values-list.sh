#!/bin/bash

# Set the base directory to this script
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
ROOT_DIR=$(dirname "$SCRIPT_DIR")

# Find all values.yaml files
files=$(find "$ROOT_DIR" -name "values.yaml")

if [[ "${#files[@]}" -lt 1 ]]; then
  echo "No values.yaml files found"
  exit 0
fi

printf "Copying docs/templates/values-list.tmpl.md markdown template to docs/values-list.md..\n\n"
cp "$ROOT_DIR/docs/templates/values-list.tmpl.md" "$ROOT_DIR/docs/values-list.md"

printf "Generating values list...\n\n"
printf "Found the following values.yaml files to process:\n%s\n" "$files"

# Loop through each file
for file in $files; do
    relative_file_path=${file/$ROOT_DIR\//}
    printf "\nGetting keys from %s...\n" "$file"

    # Extract all keys (including nested subkeys) from the file
    keys=$(yq '.. | path | join(".")' - < "$file")

    # Loop through each key
    for key in $keys; do
        # Get the value for the key
        value=$(yq -r ".$key" "$file")

        # Check if the value is not a map as that means it has subkeys we're going to read later
        if [[ "$(yq -r ".$key | kind" "$file")" != "map" ]]; then
            echo "- Adding $key to markdown table"

            # Markdown tables have issues with multiline values, plus some of these values contain backticks.
            # To simplify things for now, replace the value with something else
            if [[ "$(echo "$value" | wc -l)" -gt 1 ]]; then
              value="A multiline string, see source"
            fi

            # Add the key, value, and location to the markdown list
            printf "| \`%s\` | \`%s\` | [\`%s\`](../%s) |\n" "$key" "$value" "$relative_file_path" "$relative_file_path" >> "$ROOT_DIR/docs/values-list.md"
        fi
    done
done

printf "\ndocs/values-list.md successfully updated!"
