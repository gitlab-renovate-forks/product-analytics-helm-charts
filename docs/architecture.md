## Architecture

### High-level Overview

![High-level overview](./diagrams/img/architecture-overview.png)

### Managed service high-level overview

![Managed service High-level overview](./diagrams/img/architecture-managed-service-overview.png)
