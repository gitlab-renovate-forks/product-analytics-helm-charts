## Installing with GCP

### Prerequisites

1. You’ve cloned or downloaded this repository.
1. You have a registered domain. (You can registered using GCP [register a domain](https://cloud.google.com/domains/docs/register-domain) or a registrar of your choice)
1. Ensure you have access to the GCP project that you aim to deploy to.
   * Authenticate using `gcloud auth login`
   * Then select the project using `gcloud config set project {PROJECT_NAME}` 
   * Manually create a DNS zone that uses your domain
    * (If you are not using Cloud DNS as DNS provider for your domain) You need to change the name servers that are associated with your domain registration to point to the Cloud DNS name servers - [update your domain's name servers](https://cloud.google.com/dns/docs/update-name-servers)
   * Add DNS zone name to the `DNS_ZONE` variable in `.task-env`
1. Ensure you have [`helm`](https://formulae.brew.sh/formula/helm#default), [`google-cloud-sdk (gcloud)`](https://formulae.brew.sh/cask/google-cloud-sdk), [`go-task`](https://formulae.brew.sh/formula/go-task), and [`kubernetes-cli (kubectl)`](https://formulae.brew.sh/formula/kubernetes-cli#default) installed.
   - Set up your Google Cloud account via `gcloud init`
   - Set up the GKE auth plugin for `kubectl`:
   ```sh
   gcloud components install gke-gcloud-auth-plugin
   export USE_GKE_GCLOUD_AUTH_PLUGIN=True
   ```

### Instructions

Run the following commands in your cloned helm-charts directory. 
You can run the commands using [Taskfile](https://taskfile.dev/) to reduce effort the exact commands can be found in `Taskfile.yml`.
The following variables need to be replaced and stay the same for all commands:
   - `PROJECT_NAME`
   - `REGION`
   - `NETWORK`
   - `SUBNETWORK`
   - `CLUSTER_IPV4_CIDR`
   - `SERVICES_IPV4_CIDR`
   - `DOMAIN_NAME`
   - `DNS_ZONE`

These values can be defined within the repo roots `.task-env` file or as global environment variables. An example [`.task-env`](../.task-env.example) file can be found in this repo.
Alternatively you can provide them per command: `PROJECT_NAME="my-project" task setup-cluster`

The following variables should be provided per command but could be provided within the root `.task-env` file if you don't
have unique clusters and releases:
   - `CLUSTER_NAME`
   - `RELEASE_NAME`

### Create cluster

**Note:** Change any values in square brackets, default values are provided.

1. Create a GKE cluster. This uses autopilot clusters to not have to worry about manual resource management. It also automatically authenticates with the newly created cluster.
   ```sh
   RELEASE_NAME=[RELEASE_NAME] CLUSTER_NAME=[CLUSTER_NAME] task setup-cluster
   ```

### Using an existing cluster

**Note:** Change any values in square brackets, default values are provided.

1. Check the context that `kubectl` is currently working within:
   ```sh
   kubectl config current-context
   ```
1. If it is not the right context then check what contexts you do have:
   ```sh
   kubectl config get-context
   ```
1. If the context you need is not available, then request the credentials from GKE:
   ```sh
   gcloud container clusters get-credentials [CLUSTER_NAME] --region [REGION]
   ```
1. Find the right context and switch to it:
   ```sh
   kubectl config use-context [CONTEXT_NAME]
   ```

### Installing package requirements

1. Add chart repositories so `cert-manager` is available:
   ```sh
   task setup-helm-repos
   ```
1. Install `cert-manager` so all Ingress objects can create SSL certificates via LetsEncrypt
   ```sh
   task install-cert-manager
   ```
1. Prepare ip addresses and dns addresses for the externally reachable services in your cluster
   ```sh
   RELEASE_NAME=[RELEASE_NAME] CLUSTER_NAME=[CLUSTER_NAME] task setup-dns
   ```
1. Prepare your own `custom.values.yaml` file for the configuration of the helm chart with sane defaults.
   ```sh
   RELEASE_NAME=[RELEASE_NAME] CLUSTER_NAME=[CLUSTER_NAME] task prepare-custom-values
   ```
   * Note: You can customize the file name using the `VALUES_FILE` environment variable if you want to use a different name.
     This command will replace an existing file with the prepared custom values. If you have an existing values file, you
     can generate the custom values in a new file and copy these over to your existing file.
1. Read through your `custom.values.yml`:
    1. Check that all domains, usernames and passwords are set as expected.

### Installing a new helm chart

1. Install the chart:
   ```sh
   helm install -f custom.values.yaml [RELEASE_NAME] ./
   ```
   
### Installing with an existing helm chart

1. Add this repo to your `Chart.yaml`:
   ```yaml
   - name: gitlab-product-analytics
    repository: https://gitlab.com/api/v4/projects/55613855/packages/helm/stable
    version: x.x.x
   ```
   * Note: Update the version according to your versioning requirements.
1. Add this repo as a dependency:
   ```sh
      task add-helm-package
   ```
1. Copy the contents of `custom.values.yaml` to your existing helm chart values file
   and place it under the `gitlab-product-analytics` key. 
1. Move any `global` values from the copied contents to your existing `global` key within
   your helm chart values file. It should look like this:
   ```yaml
   global:
     ...existing global values...
     ...product analytics global values...
   existing-keys:
     ...
   gitlab-product-analytics:
     ...copied product analytics values...
   ```
1. Upgrade your existing cluster so it picks up the newly added dependency:
   ```sh
   helm upgrade -f custom.values.yaml [RELEASE_NAME] ./
   ```

### Appendix

#### Clickhouse Cloud Support

Set the following values in your `custom.values.yaml` file:

```yaml
global:
  clickhouse:
    enabled: false
    host: something.clickhouse.cloud
 tls:
    enabled: true
```

Setting `clickhouse.enabled` to `false` will disable in-cluster Clickhouse and implies that Clickhouse Cloud will be used.

#### Vector Installation

```sh
helm repo add vector https://helm.vector.dev
helm repo update
helm dependency build
```

Set values for `vector` from `example.custom.values.yaml`. At a minimum, you need to set:

- `vector.env.CLICKHOUSE_PASSWORD.valueFrom.secretKeyRef.name`
- `vector.env.KAFKA_TLS_PASSWORD.valueFrom.secretKeyRef.name`
- `vector.extraVolumes.secret.secretName`
- `vector.customConfig.sources.snowplow_enriched_events.bootstrap_server`
- `vector.customConfig.sources.snowplow_bad_events.bootstrap_server`
- `snowplow.configurator.domain`

For `CLICKHOUSE_PASSWORD` and `KAFKA_TLS_PASSWORD`, it assumes you're using a secrets management service. Otherwise, you can simply set the password via `value` instead of using `valueFrom`.

#### Let's Encrypt DNS Configuration

1. Create the Let's Encrypt DNS service account (based upon the [cert-manager guide](https://cert-manager.io/docs/configuration/acme/dns01/google/)):
   ```sh
   gcloud iam --project=[PROJECT_NAME] service-accounts create lets-encrypt-dns --display-name "Let's Encrypt DNS"
   gcloud projects add-iam-policy-binding [PROJECT_NAME] --member serviceAccount:lets-encrypt-dns@[PROJECT_NAME].iam.gserviceaccount.com --role roles/dns.admin
   ```
1. Create the service account key:
   ```sh
   gcloud iam service-accounts keys create key.json --iam-account lets-encrypt-dns@[PROJECT_NAME].iam.gserviceaccount.com
   ```
1. Add the key to your KMS provider _or_ manually save as a Kubernetes secret.

#### Prometheus Installation

See [monitoring documentation](./monitoring.md) for how to install and configure `prometheus`.

#### KMS Provider Support

##### Kafka TLS Authentication

If using a KMS provider, you will need to configure your `custom.values.yaml` with the following keys:

```yaml
global:
  clickhouse:
    auth:
      existingSecret:

certificates:
  kafka:
    keystore:
      existingSecret:
  snowplow:
    collector:
      keystore:
        existingSecret:

cube:
  secrets:
    existingSecret:

snowplow:
  configurator:
    auth:
      existingSecret:
  collector:
    keystore:
      existingSecret:

kafka:
  auth:
    tls:
      existingSecret: kafka-keystore-secret
      passwordsSecretKeystoreKey: password
      passwordsSecretTruststoreKey: password

vector:
  env:
    - name: CLICKHOUSE_PASSWORD
      valueFrom:
        secretKeyRef:
          name:
          key: password

```

Note that any defined values in `existingSecret` will override any plaintext `password` values.

#### Promoting schemas and config to Cube

Cube deployments and statefulsets are annotated with sha256 checksum. This enables deployments to automatically rollout
when a change in config/schema is detected.
Steps:
1. Make changes in cube schema by modifying one or more schema config files.
2. Follow steps in [upgrade documentation](./upgrade.md) to upgrade your release.

#### Restarting Pods

Upgraded your deployment, and changes don't look to have taken effect? You may want to check your pods configuration and see if it hasn't updated. It might need a restart via:

```sh
kubectl rollout restart deployments/statefulset [DEPLOYMENT_NAME]
```

#### Clean up cert-manager

```sh
helm uninstall cert-manager -n cert-manager
kubectl delete roles cert-manager-startupapicheck:create-cert -n cert-manager;
kubectl delete serviceaccount cert-manager-startupapicheck -n cert-manager;
kubectl delete serviceaccount default -n cert-manager;
kubectl delete jobs cert-manager-startupapicheck -n cert-manager;
kubectl delete rolebindings cert-manager-startupapicheck:create-cert -n cert-manager;
```
