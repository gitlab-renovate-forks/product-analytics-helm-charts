# Monitoring

The monitoring architecture is shown in the below diagram.

![monitoring architecture](./diagrams/img/monitoring-overview.png)

This repo is configured to use `prometheus` for monitoring. Prometheus compatible metrics are scraped from various sources and then sent to 
`mimir`. `mimir` is a [separate service](https://grafana.com/oss/mimir/) provided through Grafana and is not part of this
repo specifically.

## Flow of metrics

There are various ways of metrics being scraped and sent to `mimir`. These are outlined below.

1. Services exporting metrics for the `prometheus` server.
  Clickhouse, kafka and snowplow configurator services expose `prometheus` compatible metrics in their `/metrics` 
  endpoint. The metrics from these sources are then scraped by `prometheus` server and sent to `mimir`.
2. Exporter translating metrics not compatible with `prometheus` to `prometheus` compatible metrics.
  Snowplow collector and snowplow enricher does not expose `prometheus` compatible metrics, however, they can send 
  metrics to a `StatsD` server. Thus, we are running a `prometheus StatsD` exporter which translates StatsD metrics and 
  exposes them on a `/metrics` endpoint to be scraped by the `prometheus` server. 
3. Remote write to `mimir`.
  Vector sends metrics directly to `mimir`. In this case, `prometheus` server is not involved.
4. Kube state metrics.
  Some services does not expose any metrics data. For monitoring such services, we recommend `kube-state-metrics`. It listens 
  to the Kubernetes API server and generates metrics about the state of the objects and then exposes them to be scraped
  by `prometheus` server.

## Installation and configuration

### Prometheus installation

```shell
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm dependency build
```

Set `global.prometheus.enabled` to `true` and values for `prometheus` from `example.custom.values.yaml`. At a minimum, you need to set:

- `prometheus.serverFiles.prometheus.yml.scrape_configs.job_name`
- `prometheus.serverFiles.prometheus.yml.scrape_configs.static_configs.targets`

#### Configuring basic auth in a scrape config

Prometheus scrape config can be configured to authenticate to the server exposing metrics using basic auth.

- Fill `prometheus.serverFiles.prometheus.yml.scrape_configs.basic_auth.username`
- Fill one of
    - `prometheus.serverFiles.prometheus.yml.scrape_configs.basic_auth.password`
    - `prometheus.serverFiles.prometheus.yml.scrape_configs.basic_auth.password_file`

`password` field accepts a plain text secret and should be avoided, especially in production clusters. Instead, you can
mount a kubernetes secret to the `prometheus-server` container and use the `password_file` option. For this
- Create a kubernetes secret with name [my-kubernetes-secret-name]
- Set `extraSecretMounts` for your [my-target] like below. A target here refers to the server exposing `prometheus`
  compatible metrics on a `/metrics` endpoint.
  ```yaml
  prometheus:
    server:
      extraSecretMounts:
        - name: my-target-password
          mountPath: /etc/secrets/my-target
          subPath: ''
          secretName: my-kubernetes-secret-name
          readOnly: true
  ```
- Use the above mounted secret in the basic auth config
  ```yaml
  prometheus:
    serverFiles:
      prometheus.yml:
        scrape_configs:
          - job_name: my_scrape
            basic_auth:
              username: my-username
              password_file: /etc/secrets/my-target/password
            static_configs:
              - targets:
                - 'my-target-service-name:target-port'
                labels:
                  environment: my-env
  ```
Note that the `password_file` is mounted in the folder `mountPath` that you set in the `extraSecretMounts` to a file
with file name `key` of the kubernetes secret. In the example above, a kubernetes secret with name my-kubernetes-secret-name
has the password in the `password` key.

#### Scraping kube state metrics

Prometheus chart automatically installs `kube-state-metrics`. To disable it, set `prometheus.kube-state-metrics.enabled` to `false`.

`kube-state-metrics` by default collects metrics from all available resources. It's possible to configure it to collect metrics only from specified resources. For this set
`prometheus.kube-state-metrics.collectors`. An example configuration to expose only service metrics can be found in `example.custom.values.yaml`.

To scrape metrics exposed by `kube-state-metrics` by `prometheus`, add a new scrape config. There is an example config at the `example.custom.values.yaml`.
- Copy the configuration found at `prometheus.serverFiles.prometheus.yml.scrape_configs` with a job name `kube_state`.
- Set `targets` to [kube-state-metrics-service-name]:8080

#### Scraping kafka jmx metrics

Set `kafka.metrix.jmx.enabled` to `true` and upgrade your release. You should see a `jmx-exporter` container running in kafka pods.
You should be able to fetch the metrics in the `/metrics` endpoint in the default port of 5556.

After confirming that jmx metrics are exposed, you can configure `prometheus` to scrape those metrics. For this
- Make sure `global.prometheus.enabled` is `true`
- Add a new config to `prometheus.serverFiles.prometheus.yml.scrape_configs`. See `example.custom.values.yaml` for an example.
- In the scrape config, set `target` to [kafka-jmx-metrics-service-name]:5556

#### Scraping statsd exporter metrics

Set `global.prometheus-statsd-exporter.enabled` to `true` and upgrade your release. You should see a `[release-name]-prometheus-statsd-exporter` deployment created.
This will run a `statsd-exporter` accepting statsD traffic on port 9125 and prometheus traffic on port 9102.

Currently, `snowplow enricher` is enabled to expose statsd metrics. If you would like to enable it for `snowplow collector` you can do so by setting `snowplow.collector.monitoring.metrics.statsd.enabled` to `true`

In order to scrape these metrics from the statsd exporter, you can configure `prometheus`
- Make sure `global.prometheus.enabled` is `true`
- Add a new config to `prometheus.serverFiles.prometheus.yml.scrape_configs`. See `statsd_exporter` job in `example.custom.values.yaml` for an example.
- In the scrape config, set `target` to [prometheus-statsd-exporter-service-name]:9102

#### Scraping analytics configurator metrics

Starting from version 2.4.1 analytics configurator exposes `prometheus` compatible metrics. In order to scrape these metrics, you can configure `prometheus`
- Make sure `global.prometheus.enabled` is `true`
- Add a new config to `prometheus.serverFiles.prometheus.yml.scrape_configs`. See `analytics_configurator` job in `example.custom.values.yaml` for an example.
- In the scrape config, set `target` to [snowplow-configurator-service-name]:4567
- In the scrape config, fill [basic_auth field](#configuring-basic-auth-in-a-scrape-config)

#### Scraping Stackdriver Exporter metrics

In order to set up Stackdriver Exporter
- set `global.prometheus-stackdriver-exporter.enabled` to true.
- set `prometheus-stackdriver-exporter.stackdriver`. See `example.custom.values.yaml` for an example.
- upgrade your release. You should see a new `prometheus-stackdriver-exporter` pod running.

To scrape the `prometheus-stackdriver-exporter` metrics
- Make sure `global.prometheus.enabled` is `true`
- Add a new config to `prometheus.serverFiles.prometheus.yml.scrape_configs`. See `stackdriver_exporter` job in `example.custom.values.yaml` for an example.
- In the scrape config, set `target` to [prometheus-statsd-exporter-service-name]:9255

### Alerting on log-based metrics

Alerts can be configured for any metric available in `Mimir` which is the endpoint for all our metrics sent from `prometheus-server`.
Alerts can also be configured based on logs. For this, we recommend the following approach:

- Exporting log based metrics from GCP. This can be configured in the GCP console on [its relevant page](https://console.cloud.google.com/logs/metrics?referrer=search).
- Since log-based metrics from `GCP` are not `prometheus` compatible, we are running `stackdriver-exporter` in each our
- cluster to translate those metrics to `prometheus` compatible metrics.
- `Stackdriver-exporter` in each cluster is only exposing the metrics that belong to logs from that cluster. That's achieved by
passing a metric filter in the `stackdriver-exporter` config. See `prometheus-stackdriver-exporter.stackdriver.metrics.filters`
in the `example.custom.values.yaml` file for an example.
- The metrics from `stackdriver-exporter` are then scraped by `prometheus-server`. See `stackdriver_exporter` job in the
`example.custom.values.yaml` for an example scrape config.
