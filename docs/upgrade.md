## Upgrading with GCP

### Prerequisites

See the [installation prerequisites](./installation.md#prerequisites).

1. Make your changes to the helm chart, templates, or `*.values.yaml` definitions.

### Instructions

**Note:** Change any values in square brackets, default values are provided.

1. Check the context that `kubectl` is currently working within:
   - `kubectl config current-context`
1. If it is not the right context then check what contexts you do have:
   - `kubectl config get-contexts`
1. If the context you need is not available, then request the credentials from GKE:
   - `gcloud container clusters get-credentials [CLUSTER_NAME:my-product-analytics-cluster] --region [REGION:us-east1]`
1. Find the right context and switch to it:
   - `kubectl config use-context [CONTEXT_NAME]`
1. **All `helm` related commands will be run through this context.**
1. *(Optional)* Dry run the changes `helm` is going to make to verify before deployment:
   - `helm upgrade -f [VALUES_YAML:custom.values.yaml] [RELEASE_NAME:my-gitlab-analytics-stack] . --dry-run`
1. Run the upgrade:
    - `helm upgrade -f [VALUES_YAML:custom.values.yaml] [RELEASE_NAME:my-gitlab-analytics-stack] .`
1. *(Optional)* Use the helm diff plugin to validate the changes in the latest revision:
    - Make a note of the revision number output by the `helm upgrade` command
    - Install the plugin: `helm plugin install https://github.com/databus23/helm-diff`
    - Compare the new revision against the previous one (or older): `helm diff revision [RELEASE_NAME:my-gitlab-analytics-stack] [PREVIOUS_REVISION:1] [NEW_REVISION:2]`
1. Verify the changes have been applied to the deployments by running:
   - `kubectl get deployments`
1. If all the deployments have an age older than when you deployed the upgrade, give it time. If needed you can rollout a new deployment:
   - `kubectl rollout restart deployments [DEPLOYMENT_NAME]`
1. You can check the events on a specific pod within the deployment:
   - Get the pods: `kubectl get pods`
   - Check the pods events: `kubectl describe pods [POD_NAME]`
   - This will tell you if there are any errors as the pod is created and gets up and running


### Appendix

#### What to do if a deployment doesn't run?

Sometimes a deployment gets stuck because a previous ReplicaSet hasn't relinquished all it's resources.
This often happens when a previous ReplicaSet still holds a persistent volume claim.

To resolve this you need to remove it:

1. Find all the ReplicaSets:
   - `kubectl get replicaset`
1. Note the name of any ReplicaSets that should have no resources assigned and delete them:
   - `kubectl delete [REPLICASET_NAME]`

This will delete the old ReplicaSet and cause it to garbage collect on all its resources.
Once it has garbage collected, your new deployments ReplicaSet will create new resources to meet the requirements set
out by the helm chart.
