# Troubleshooting

## Endpoints returning `ERR_CERT_AUTHORITY_INVALID`

Sometimes when re-installing the chart, old secrets for Let's Encrypt may be used.

Check via `kubectl get secret` and `kubectl get ingress` to see if the secrets are older than the ingresses.

If this is the case, delete the secrets, and the ClusterIssuer should requeue for new certificates to be issued.

## Running queries on Clickhouse pod

When running in SSL, you need to run `update-ca-certificates` on the pod in order to update the certificate chain.

Afterwards, you should be able to run `clickhouse client --user "$USER" --port 9440 --secure --password` to perform local queries.
