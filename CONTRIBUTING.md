# Contribution guidelines

[TOC]

## Contribute to GitLab Product Analytics Helm Charts

Thank you for your interest in contributing to the GitLab Product Analytics Helm Charts. This guide details how
to contribute to GitLab in a way that is efficient for everyone.

## Contributor License Agreement and Developer Certificate of Origin

Contributions to this repository are subject to the [Developer Certificate of Origin](doc/legal/developer_certificate_of_origin.md#developer-certificate-of-origin-version-11).

All Documentation content that resides under the [`docs/` directory](/docs) of this
repository is licensed under Creative Commons:
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

## Code of Conduct

As contributors and maintainers of this project, we pledge to respect all
people who contribute through reporting issues, posting feature requests,
updating documentation, submitting merge requests or patches, and other
activities.

We are committed to making participation in this project a harassment-free
experience for everyone, regardless of age, body size, disability, ethnicity,
sex characteristics, gender identity and expression, level of experience,
education, socio-economic status, nationality, personal appearance, race,
religion, or sexual identity and orientation.

Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct. Project maintainers who do not
follow the Code of Conduct may be removed from the project team.

This code of conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community.

Instances of abusive, harassing, or otherwise unacceptable behavior can be
reported by emailing `contact@gitlab.com`.

This Code of Conduct is adapted from the [Contributor Covenant](http://contributor-covenant.org),
version 1.1.0, available at [http://contributor-covenant.org/version/1/1/0/](http://contributor-covenant.org/version/1/1/0/).

## Getting Started

For help setting up this repository, see the [README](./README.md).

### Reporting Issues

Create a [new issue from the "Bug" template](https://gitlab.com/gitlab-org/analytics-section/product-analytics/helm-charts/-/issues/new?issuable_template=Bug)
and follow the instructions in the template.

### Proposing Features

Create a
[new issue from the "Feature Proposal" template](https://gitlab.com/gitlab-org/analytics-section/product-analytics/helm-charts/-/issues/new?issuable_template=Feature%20Proposal)
and follow the instructions in the template.

### Versioning

Each new MR requires an update to the [`Chart.yml`](./Chart.yaml) version number. The new version should follow [semver versioning practices](https://semver.org/).

If an MR is only updating: documentation, pipelines, or other repository configuration files; then you can
apply the label ~"pipeline:skip-chart-version-check" to the MR. This label will skip the pipeline check for a new chart
version. Reviewers should make note of the use of this label and whether it is appropriate for the MR they are reviewing.

### Documenting values

Helm charts make extensive use of `values.yaml` files. The values of a dependency can be changed by setting the
value in a `values.yaml` or `custom.values.yaml` file within the repo. We make use of this ability to set a
`custom.values.yaml` file, and customize the values according to the deployment.

To make it easier for others to find the values that can be changed, we generate a [list of available values](./docs/values-list.md).
This list is not exhaustive, there are values that could be set specific to the environment the helm chart is deployed
to which this repo doesn't assign by default. Review your environments documentation for additional properties that
might be customizable.

This list needs to be updated whenever a `values.yaml` file is changed within this repo. To update the values list,
run: `./scripts/generate-values-list.sh`.

If a values change produces an identical `values-list.md`, you can skip this check by applying the label
~"pipeline:skip-values-list-check" to the MR.

### Releases

When a new version is merged into the `main` branch, it will automatically generate a new release. The new release will
contain:

- The new version number
- The commits related to the release
- A new helm package related to the version

Users can update their usage of this package by running `helm dependencies update` on their helm instance.
